(defsystem "assets"
  :version "0.1.0"
  :author "ekatsym"
  :license "LLGPL"
  :depends-on ()
  :components ((:module "src"
                :components
                ((:file "package")
                 (:file "defmacro"          :depends-on ("package"))
                 (:file "list"              :depends-on ("package"))
                 (:file "function"          :depends-on ("package"))
                 (:file "lazy"              :depends-on ("package"))
                 (:file "llist"             :depends-on ("package" "lazy"))
                 (:file "queue"             :depends-on ("package" "llist"))
                 (:file "anaphora"          :depends-on ("package"))
                 (:file "nondeterminism"    :depends-on ("package"))
                 (:file "comprehension"     :depends-on ("package"))
                 (:file "monad"             :depends-on ("package"))
                 )))
  :description ""
  :in-order-to ((test-op (test-op "assets/tests"))))

(defsystem "assets/tests"
  :author "ekatsym"
  :license "LLGPL"
  :depends-on ("assets"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for assets"
  :perform (test-op (op c) (symbol-call :rove :run c)))
