(defpackage assets.defmacro
  (:use :common-lisp)
  (:import-from :assets
                #:defmacro!)
  (:export #:defmacro!))
(in-package :assets.defmacro)

(defmacro defmacro/g! (name lambda-list &body body)
  (flet ((flatten (x)
           (labels ((rec (x acc)
                      (cond ((null x) acc)
                            #+sbcl ((sb-impl::comma-p x) (rec (sb-impl::comma-expr x) acc))
                            #+sbcl ((eq x 'sb-int:quasiquote) acc)
                            ((atom x) (cons x acc))
                            (t (rec (car x) (rec (cdr x) acc))))))
             (rec x '())))
         (g!symbol? (x)
           (and (symbolp x)
                (let ((name (symbol-name x)))
                  (and (> (length name) 2)
                       (char= (char name 0) #\G)
                       (char= (char name 1) #\!))))))
    (let ((g!symbols (remove-duplicates
                       (remove-if-not #'g!symbol?
                                      (flatten body)))))
      `(defmacro ,name ,lambda-list
         (let ,(mapcar (lambda (g!sym)
                         `(,g!sym (gensym ,(string g!sym))))
                       g!symbols)
           ,@body)))))

(defmacro defmacro! (name lambda-list &body body)
  (flet ((flatten (x)
           (labels ((rec (x acc)
                      (cond ((null x) acc)
                            #+sbcl ((sb-impl::comma-p x) (rec (sb-impl::comma-expr x) acc))
                            #+sbcl ((eq x 'sb-int:quasiquote) acc)
                            ((atom x) (cons x acc))
                            (t (rec (car x) (rec (cdr x) acc))))))
             (rec x '())))
         (o!symbol? (x)
           (and (symbolp x)
                (let ((name (symbol-name x)))
                  (and (> (length name) 2)
                       (char= (char name 0) #\O)
                       (char= (char name 1) #\!)))))
         (o!->g!symbol (o!sym)
           (identity
             (intern
               (concatenate
                 'string "G!" (subseq (symbol-name o!sym) 2))))))
    (let* ((o!symbols (remove-duplicates
                        (remove-if-not #'o!symbol?
                                       (flatten lambda-list))))
           (g!symbols (mapcar #'o!->g!symbol o!symbols)))
      `(defmacro/g! ,name ,lambda-list
         `(let ,(mapcar #'list (list ,@g!symbols) (list ,@o!symbols))
            ,(progn ,@body))))))
