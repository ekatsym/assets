(defpackage assets.llist
  (:use :common-lisp)
  (:import-from :assets
                #:promise #:delay #:force
                #:llist #:lendp
                #:lcons #:lcar #:lcdr
                #:lfirst #:lrest
                #:llist->list #:list->llist
                #:lreverse #:lappend #:lrevappend
                #:ltake #:ldrop #:lsublist
                #:lnth #:linit #:llast
                #:lfoldl #:lfoldr #:lunfoldl #:lunfoldr
                #:lmap #:lfilter
                #:lremove #:lremove-if #:lremove-if-not
                #:lmember #:lmember-if
                #:lsplit #:lsplit-if #:lsplit-at
                #:ltree->tree #:tree->ltree)
  (:export #:promise #:delay #:force
           #:llist #:lendp
           #:lcons #:lcar #:lcdr
           #:lfirst #:lrest
           #:llist->list #:list->llist
           #:lreverse #:lappend #:lrevappend
           #:ltake #:ldrop #:lsublist
           #:lnth #:linit #:llast
           #:lfoldl #:lfoldr #:lunfoldl #:lunfoldr
           #:lmap #:lfilter
           #:lremove #:lremove-if #:lremove-if-not
           #:lmember #:lmember-if
           #:lsplit #:lsplit-if #:lsplit-at
           #:ltree->tree #:tree->ltree))

(in-package :assets.llist)

(deftype index ()
  `(integer 0 ,array-dimension-limit))

(deftype llist ()
  '(or promise null))

(declaim (inline lendp))
(defun lendp (object)
  (check-type object llist)
  (null object))

(defmacro lcons (se1 se2)
  `(delay (cons ,se1 ,se2)))

(defun lcar (llist)
  (if (lendp llist)
      '()
      (car (force llist))))

(declaim (inline lfirst))
(defun lfirst (llist)
  (lcar llist))

(defun lcdr (llist)
  (if (lendp llist)
      '()
      (cdr (force llist))))

(declaim (inline lrest))
(defun lrest (llist)
  (lcdr llist))

(defmacro llist (&rest args)
  (reduce (lambda (x acc) `(lcons ,x ,acc))
          args
          :initial-value ''()
          :from-end t))

(defun llist->list (llist)
  (check-type llist llist)
  (do ((llst llist (lrest llst))
       (acc '() (cons (lfirst llst) acc)))
      ((lendp llst) (nreverse acc))))

(defun list->llist (list)
  (check-type list list)
  (reduce (lambda (x acc) (lcons x acc))
          list
          :initial-value '()
          :from-end t))

(defun lappend (&rest llists)
  (assert (every (lambda (x) (typep x 'llist)) llists))
  (labels ((lapp2 (llst1 llst2)
             (declare (optimize (speed 3)))
             (if (lendp llst1)
                 llst2
                 (lcons (lfirst llst1) (lapp2 (lrest llst1) llst2)))))
    (reduce #'lapp2 llists
            :from-end t)))

(defun lreverse (llist)
  (check-type llist llist)
  (labels ((rec (llst acc)
             (if (lendp llst)
                 acc
                 (rec (lrest llst) (lcons (lfirst llst) acc)))))
    (rec llist '())))

(defun lrevappend (llist1 llist2)
  (check-type llist1 llist)
  (check-type llist2 llist)
  (labels ((rec (llst1 llst2)
             (declare (optimize (speed 3)))
             (if (lendp llst1)
                 llst2
                 (rec (lrest llst1) (lcons (lfirst llst1) llst2)))))
    (rec llist1 llist2)))

(defun ltake (n llist)
  (check-type llist llist)
  (check-type n index)
  (if (zerop n)
      '()
      (lcons (lfirst llist) (ltake (1- n) (lrest llist)))))

(defun ldrop (n llist)
  (check-type llist llist)
  (check-type n index)
  (if (zerop n)
      llist
      (ldrop (1- n) (lrest llist))))

(defun lsublist (llist start &optional end)
  (check-type llist llist)
  (check-type start index)
  (check-type end (or null index))
  (when end
    (assert (<= start end) (start end)))
  (if (null end)
      (ldrop start llist)
      (ltake (- end start) (ldrop start llist))))

(defun lnth (n llist)
  (check-type n index)
  (check-type llist llist)
  (do ((i n (1- i))
       (llst llist (lrest llst)))
      ((zerop i) (lfirst llst))))

(defun linit (llist &optional (n 1))
  (check-type llist llist)
  (check-type n index)
  (if (lendp (ldrop n llist))
      '()
      (lcons (lfirst llist) (linit (lrest llist) n))))

(defun llast (llist &optional (n 1))
  (declare (optimize (speed 3)))
  (check-type llist llist)
  (check-type n index)
  (if (lendp (ldrop n llist))
      llist
      (llast (lrest llist) n)))

(defun lfoldl (function x llist &rest more-llists)
  (check-type function function)
  (check-type llist llist)
  (assert (every (lambda (x) (typep x 'llist)) more-llists) (more-llists))
  (if (null more-llists)
      (do ((llst llist (lrest llst))
           (acc x (funcall function (lfirst llst) acc)))
          ((lendp llst) acc))
      (do ((llsts (cons llist more-llists) (mapcar #'lrest llsts))
           (acc x (apply function (append (mapcar #'lfirst llsts) (list acc)))))
          ((some #'lendp llsts) acc))))

(defun lfoldr (function x llist &rest more-llists)
  (check-type function function)
  (check-type llist llist)
  (assert (every (lambda (x) (typep x 'llist)) more-llists) (more-llists))
  (labels ((lfr1 (llst)
             (if (lendp llst)
                 x
                 (funcall function
                          (lfirst llst)
                          (lfr1 (lrest llst)))))
           (lfrs (llsts)
             (if (some #'lendp llsts)
                 x
                 (apply function
                        (append
                          (mapcar #'lfirst llsts)
                          (list (lfrs (mapcar #'lrest llsts))))))))
    (if (null more-llists)
        (lfr1 llist)
        (lfrs (cons llist more-llists)))))

(defun lunfoldl (predicate function successor seed &optional tail-generator)
  (check-type predicate function)
  (check-type function function)
  (check-type successor function)
  (check-type tail-generator (or null function))
  (labels ((rec (x)
             (if (funcall predicate x)
                 (if (null tail-generator)
                     '()
                     (funcall tail-generator x))
                 (lcons (funcall function x)
                        (rec (funcall successor x))))))
    (rec seed)))

(defun lunfoldr (predicate function successor seed &optional (tail '()))
  (check-type predicate function)
  (check-type function function)
  (check-type successor function)
  (labels ((rec (x acc)
             (declare (optimize (speed 3))
                      (type function function))
             (if (funcall predicate x)
                 acc
                 (rec (funcall successor x)
                      (lcons (funcall function x) acc)))))
    (rec seed tail)))

(defun lmap (function llist &rest more-llists)
  (check-type function function)
  (check-type llist llist)
  (assert (every (lambda (x) (typep x 'llist)) more-llists) (more-llists))
  (labels ((lmp1 (llst)
             (declare (optimize (speed 3))
                      (type function function))
             (if (lendp llst)
                 '()
                 (lcons (funcall function (lfirst llst))
                        (lmp1 (lrest llst)))))
           (lmps (llsts)
             (declare (optimize (speed 3))
                      (type function function))
             (if (some #'lendp llsts)
                 '()
                 (lcons (apply function (mapcar #'lfirst llsts))
                        (lmps (mapcar #'lrest llsts))))))
    (if (null more-llists)
        (lmp1 llist)
        (lmps (cons llist more-llists)))))

(defun lfilter (function llist &rest more-llists)
  (check-type function function)
  (check-type llist llist)
  (assert (every (lambda (x) (typep x 'llist)) more-llists) (more-llists))
  (labels ((lmp1 (llst)
             (declare (optimize (speed 3))
                      (type function function))
             (if (lendp llst)
                 '()
                 (let ((x (funcall function (lfirst llst))))
                   (if (null x)
                       (lmp1 (lrest llst))
                       (lcons x (lmp1 (lrest llst)))))))
           (lmps (llsts)
             (declare (optimize (speed 3))
                      (type function function))
             (if (some #'lendp llsts)
                 '()
                 (let ((x (apply function (mapcar #'lfirst llsts))))
                   (if (null x)
                       (lmps (mapcar #'lrest llsts))
                       (lcons x (lmps (mapcar #'lrest llsts))))))))
    (if (null more-llists)
        (lmp1 llist)
        (lmps (cons llist more-llists)))) )

(defun lremove-if (function llist)
  (check-type function function)
  (check-type llist llist)
  (labels ((rec (llst)
             (declare (optimize (speed 3))
                      (type function function))
             (cond ((lendp llst)
                    '())
                   ((funcall function (lfirst llst))
                    (rec (lrest llst)))
                   (t
                    (lcons (lfirst llst) (rec (lrest llst)))))))
    (rec llist)))

(declaim (inline lremove-if-not))
(defun lremove-if-not (function llist)
  (lremove-if (complement function) llist))

(defun lmember (item llist &key (test #'eql))
  (check-type llist llist)
  (check-type test function)
  (do ((llst llist (lrest llst)))
      ((lendp llst) nil)
      (when (funcall test (lfirst llst) item)
        (return llst))))

(defun lmember-if (predicate llist)
  (check-type predicate function)
  (check-type llist llist)
  (do ((llst llist (lrest llst)))
      ((lendp llst) nil)
      (when (funcall predicate (lfirst llst))
        llst)))

(defun lsplit (item llist &key count (test #'eql))
  (check-type llist llist)
  (check-type count (or null (and index (integer 1 *))))
  (check-type test function)
  (labels ((lspl (llst tmp acc)
             (declare (optimize (speed 3)))
             (cond ((lendp llst)
                    (lreverse (lcons (lreverse tmp) acc)))
                   ((funcall test (lfirst llst) item)
                    (lspl (lrest llst) '() (lcons (lreverse tmp) acc)))
                   (t
                    (lspl (lrest llst) (lcons (lfirst llst) tmp) acc))))
           (lspln (n llst tmp acc)
             (declare (optimize (speed 3))
                      (type index n))
             (cond ((zerop n)
                    (lreverse (lcons (lrevappend tmp llst) acc)))
                   ((or (lendp llst) (funcall test (lfirst llst) item))
                    (lspln (1- n) (lrest llst) '() (lcons (lreverse tmp) acc)))
                   (t
                    (lspln n (lrest llst) (lcons (lfirst llst) tmp) acc)))))
    (if (null count)
        (lspl llist '() '())
        (lspln (1- count) llist '() '()))))

(defun lsplit-if (predicate llist &key count)
  (check-type predicate function)
  (check-type llist llist)
  (check-type count (or null (and index (integer 1 *))))
  (labels ((lspl (llst tmp acc)
             (declare (optimize (speed 3)))
             (cond ((lendp llst)
                    (lreverse (lcons (lreverse tmp) acc)))
                   ((funcall predicate (lfirst llst))
                    (lspl (lrest llst) '() (lcons (lreverse tmp) acc)))
                   (t
                    (lspl (lrest llst) (lcons (lfirst llst) tmp) acc))))
           (lspln (n llst tmp acc)
             (declare (optimize (speed 3))
                      (type index n))
             (cond ((zerop n)
                    (lreverse (lcons (lrevappend tmp llst) acc)))
                   ((or (lendp llst) (funcall predicate (lfirst llst)))
                    (lspln (1- n) (lrest llst) '() (lcons (lreverse tmp) acc)))
                   (t
                    (lspln n (lrest llst) (lcons (lfirst llst) tmp) acc)))))
    (if (null count)
        (lspl llist '() '())
        (lspln (1- count) llist '() '()))))

(defun lsplit-at (index llist)
  (check-type llist llist)
  (check-type index index)
  (llist (ltake index llist)
         (ldrop index llist)))

(defun ltree->tree (ltree)
  (check-type ltree llist)
  (labels ((rec (ltr acc)
             (declare (optimize (speed 3)))
             (cond ((lendp ltr)
                    (nreverse acc))
                   ((typep (lfirst ltr) 'llist)
                    (rec (lrest ltr) (cons (rec (lfirst ltr) '()) acc)))
                   (t
                    (rec (lrest ltr) (cons (lfirst ltr) acc))))))
    (rec ltree '())))

(defun tree->ltree (tree)
  (check-type tree list)
  (labels ((rec (tr acc)
             (declare (optimize (speed 3)))
             (cond ((endp tr) (lreverse acc))
                   ((typep (first tr) 'llist)
                    (rec (rest tr) (lcons (rec (first tr) '()) acc)))
                   (t
                    (rec (rest tr) (lcons (first tr) acc))))))
    (rec tree '())))
