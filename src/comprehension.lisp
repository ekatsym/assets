(defpackage assets.comprehension
  (:use :common-lisp)
  (:import-from :assets
                #:list-comprehension
                #:in #:is #:for #:to)
  (:export #:list-comprehension
           #:in #:is #:for #:to
           #:upto #:downto))
(in-package :assets.comprehension)

(defmacro list-comprehension (element &body clauses)
  (let ((g!acc (gensym "G!ACC")))
    `(let ((,g!acc '()))
       ,(parse-clauses element g!acc clauses)
       (nreverse ,g!acc))))

(defun parse-clauses (element acc clauses)
  (if (null clauses)
      `(push ,element ,acc)
      (parse-clause
        (first clauses)
        (parse-clauses element acc (rest clauses)))))

(defun parse-clause (clause body)
  (case (first clause)
    ((in)
     (ecase (length clause)
       ((3)
        (destructuring-bind (var expr) (rest clause)
          `(dolist (,var ,expr)
             ,body)))
       ((4)
        (destructuring-bind (var start end) (rest clause)
          `(do ((,var ,start (1+ ,var)))
               ((> ,var ,end))
               ,body)))))

    ((is)
     (destructuring-bind (var expr) (rest clause)
       `(let ((,var ,expr))
          ,body)))

    ((for)
     (destructuring-bind (var start to end) (rest clause)
       (ecase to
         ((to upto)
          `(do ((,var ,start (1+ ,var)))
               ((> ,var ,end))
               ,body))
         ((downto)
          `(do ((,var ,start (1- ,var)))
               ((< ,var ,end))
               ,body)))))

    (otherwise
      `(when ,clause
         ,body))))
