(defpackage assets.queue
  (:use :common-lisp)
  (:import-from :assets
                #:llist #:lcons #:lfirst #:lrest
                #:lappend #:lreverse
                #:llist->list

                #:queue #:lqueue
                #:*qempty* #:*lqempty*
                #:qempty? #:qsnoc #:qfirst #:qrest)
  (:export #:queue #:lqueue
           #:*qempty* #:*lqempty*
           #:qempty? #:qsnoc #:qfirst #:qrest))
(in-package :assets.queue)


;;; Utility
(deftype index ()
  `(integer 0 ,array-dimension-limit))


;;; Generic Functions
(defgeneric qempty? (queue)
  (:documentation "Check whether QUEUE is empty or not."))

(defgeneric qcheck (queue)
  (:documentation "Ensure the QUEUE's Q1 slot keeps being not '() when QUEUE is not empty."))

(defgeneric qsnoc (queue x)
  (:documentation "Add X into last of QUEUE."))

(defgeneric qfirst (queue)
  (:documentation "Return a first element of QUEUE"))

(defgeneric qrest (queue)
  (:documentation "Return the rest QUEUE removed a first element."))


;;; Normal Queue
(defstruct (queue (:constructor make-queue (q1 q2)))
  (q1 q1 :type list :read-only t)
  (q2 q2 :type list :read-only t))

(defmethod print-object ((object queue) stream)
  (format stream "#<QUEUE ~a>"
          (append (queue-q1 object)
                  (reverse (queue-q2 object)))))

(defparameter *qempty*
  (make-queue '() '()))

(defmethod qempty? ((queue queue))
  (null (queue-q1 queue)))

(defmethod qcheck ((queue queue))
  (if (and (null (queue-q1 queue))
           (queue-q2 queue))
      (make-queue (reverse (queue-q2 queue)) '())
      queue))

(defmethod qsnoc ((queue queue) x)
  (qcheck
    (make-queue (queue-q1 queue)
                (cons x (queue-q2 queue)))))

(defmethod qfirst ((queue queue))
  (first (queue-q1 queue)))

(defmethod qrest ((queue queue))
  (qcheck
    (make-queue (rest (queue-q1 queue))
                (queue-q2 queue))))


;;; Lazy Queue
(defstruct (lqueue (:constructor make-lqueue (q1 q2 nq1 nq2)))
  (q1   q1  :type llist :read-only t)
  (q2   q2  :type llist :read-only t)
  (nq1  nq1 :type index :read-only t)
  (nq2  nq2 :type index :read-only t))

(defmethod print-object ((object lqueue) stream)
  (format stream "#<LQUEUE ~a>"
          (append (llist->list (lqueue-q1 object))
                  (reverse (llist->list (lqueue-q2 object))))))

(defparameter *lqempty*
  (make-lqueue '() '() 0 0))

(defmethod qempty? ((queue lqueue))
  (zerop (lqueue-nq1 queue)))

(defmethod qcheck ((queue lqueue))
  (with-slots (q1 q2 nq1 nq2) queue
    (if (<= nq2 nq1)
        queue
        (make-lqueue (lappend q1 (lreverse q2))
                     '()
                     (+ nq1 nq2)
                     0))))

(defmethod qsnoc ((queue lqueue) x)
  (with-slots (q1 q2 nq1 nq2) queue
    (qcheck (make-lqueue q1 (lcons x q2) nq1 (1+ nq2)))))

(defmethod qfirst ((queue lqueue))
  (lfirst (lqueue-q1 queue)))

(defmethod qrest ((queue lqueue))
  (with-slots (q1 q2 nq1 nq2) queue
    (qcheck (make-lqueue (lrest q1) q2 (1- nq1) nq2))))
