(defpackage assets.function
  (:use :common-lisp)
  (:import-from :assets
                #:compose #:composen
                #:partial #:rpartial
                #:curry #:rcurry
                #:pipe
                #:f-if #:f-and #:f-or
                #:recur #:trecur)
  (:export #:compose #:composen
           #:partial #:rpartial
           #:curry #:rcurry
           #:pipe
           #:f-if #:f-and #:f-or
           #:recur #:trecur))
(in-package :assets.function)

(deftype index ()
  `(integer 0 ,array-dimension-limit))

(defun nlist? (n object)
  (check-type n index)
  (do ((i n (1- i))
       (x object (rest x)))
      ((zerop i) (null x))
      (when (endp x)
        (return nil))))

(defun compose (&rest functions)
  (assert (every #'functionp functions) (functions))
  (labels ((rec (fs)
             (cond ((nlist? 1 fs)
                    (first fs))
                   (t
                    (let ((composed (rec (rest fs))))
                      (lambda (&rest args)
                        (multiple-value-call (first fs)
                          (apply composed args))))))))
    (if (null functions)
        #'values
        (rec functions))))

(defun composen (n function)
  (check-type n index)
  (if (zerop n)
      #'values
      (do ((i n (1- i))
           (f function (compose f function)))
          ((zerop i) f))))

(defun partial (function &rest args)
  (check-type function function)
  (lambda (&rest rest-args)
    (apply function (append args rest-args))))

(defun rpartial (function &rest args)
  (check-type function function)
  (lambda (&rest rest-args)
    (apply function (append rest-args args))))

(defun curry (function)
  (check-type function function)
  (lambda (&rest args)
    (lambda (&rest rest-args)
      (apply function (append args rest-args)))))

(defun rcurry (function)
  (check-type function function)
  (lambda (&rest args)
    (lambda (&rest rest-args)
      (apply function (append rest-args args)))))

(defun pipe (x &rest functions)
  (assert (every #'functionp functions) (functions))
  (do ((fs functions (rest fs))
       (x x (funcall (first fs) x)))
      ((null fs) x)))

(defun f-if (ftest fthen felse)
  (check-type ftest function)
  (check-type fthen function)
  (check-type felse function)
  (lambda (&rest args)
    (if (apply ftest args)
        (apply fthen args)
        (apply felse args))))

(defun f-and (&rest functions)
  (assert (every #'functionp functions) (functions))
  (labels ((rec (fs)
             (cond ((nlist? 1 fs)
                    (first fs))
                   (t
                    (let ((chain (rec (rest fs))))
                      (lambda (&rest args)
                        (and (apply (first fs) args)
                             (apply chain args))))))))
    (if (null functions)
        (lambda (&rest args)
          (declare (ignore args))
          t)
        (rec functions))))

(defun f-or (&rest functions)
  (assert (every #'functionp functions) (functions))
  (labels ((rec (fs)
             (cond ((nlist? 1 fs)
                    (first fs))
                   (t
                    (let ((chain (rec (rest fs))))
                      (lambda (&rest args)
                        (or (apply (first fs) args)
                            (apply chain args))))))))
    (if (null functions)
        (lambda (&rest args)
          (declare (ignore args))
          nil)
        (rec functions))))

(defun recur (null1 nil2 cons2 car1 cdr1)
  (check-type null1 function)
  (check-type car1 function)
  (check-type cdr1 function)
  (check-type cons2 function)
  (labels ((rec (x)
             (if (funcall null1 x)
                 nil2
                 (funcall cons2
                          (funcall car1 x)
                          (rec (funcall cdr1 x))))))
    #'rec))

(defun tail-recur (null1 nil2 cons2 car1 cdr1)
  (check-type null1 function)
  (check-type car1 function)
  (check-type cdr1 function)
  (check-type cons2 function)
  (labels ((rec (x acc)
             (declare (optimize (speed 3))
                      (type function null1 car1 cdr1 cons2))
             (if (funcall null1 x)
                 acc
                 (rec (funcall cdr1 x)
                      (funcall cons2 (funcall car1 x) acc)))))
    (lambda (x) (rec x nil2))))
