(defpackage assets.anaphora
  (:use :common-lisp)
  (:import-from :assets
                #:aif #:sif
                #:awhen #:swhen
                #:aunless #:sunless
                #:acond #:scond
                #:acase #:scase
                #:aecase #:secase
                #:atypecase #:stypecase
                #:aetypecase #:setypecase
                #:self #:alambda)
  (:export #:it
           #:aif #:sif
           #:awhen #:swhen
           #:aunless #:sunless
           #:acond #:scond
           #:acase #:scase
           #:aecase #:secase
           #:atypecase #:stypecase
           #:aetypecase #:setypecase
           #:self #:alambda))
(in-package :assets.anaphora)


;;; Util
(deftype index ()
  `(integer 0 ,array-dimension-limit))

(defun nlist? (n object)
  (check-type n index)
  (do ((i n (1- i))
       (lst object (rest lst)))
      ((zerop i) (null lst))
      (when (null lst)
        (return nil))))

(declaim (inline ensure-list))
(defun ensure-list (x)
  (if (listp x) x (list x)))

(defun mappend (f lst)
  (do ((l lst (rest l))
       (acc '() (cons (funcall f (first l)) acc)))
      ((endp l) (reduce (lambda (acc x) (append x acc)) acc))))

(declaim (inline compose))
(defun compose (g f)
  (lambda (&rest args)
    (funcall g (apply f args))))


;;; Helper
(defun parse-cond-clauses (cond if clauses)
  (if (null clauses)
      'nil
      (let ((clause1 (first clauses))
            (clauses (rest clauses)))
        (if (atom clause1)
            (error 'simple-type-error
                   :format-control "~A clause is not a ~S: ~S"
                   :format-arguments (list cond 'cons clause1)
                   :expected-type 'cons
                   :datum clause1)
            `(,if ,(first clause1)
                  ,(case (length clause1)
                     (1 'it)
                     (2 (second clause1))
                     (otherwise `(progn ,@(rest clause1))))
                  ,(parse-cond-clauses cond if clauses))))))

(defun parse-case-clause (clause)
  (check-type clause cons)
  (let ((choices (first clause))
        (body (rest clause)))
    (cond ((eq choices 'otherwise)
           `(t ,@body))
          ((atom choices)
           `((eql it ,choices) ,@body))
          ((nlist? 1 choices)
           `((eql it ,@choices) ,@body))
          (t
           `((or ,@(mapcar (lambda (c) `(eql it ,c)) choices)) ,@body)))))

(defun parse-typecase-clause (clause)
  (check-type clause cons)
  (let ((choices (first clause))
        (body (rest clause)))
    (if (eq choices 'otherwise)
        `(t ,@body)
        `((typep it ',choices) ,@body))))


;;; IT Anaphoras
(defmacro aif (test then &optional else)
  `(let ((it ,test))
     (if it ,then ,else)))

(defmacro sif (test then &optional else)
  `(symbol-macrolet ((it ,test))
     (if it ,then ,else)))

(defmacro awhen (test &body forms)
  `(let ((it ,test))
     (when it ,@forms)))

(defmacro swhen (test &body forms)
  `(symbol-macrolet ((it ,test))
     (when it ,@forms)))

(defmacro aunless (test &body forms)
  `(let ((it ,test))
     (unless it ,@forms)))

(defmacro sunless (test &body forms)
  `(symbol-macrolet ((it ,test))
     (unless it ,@forms)))

(defmacro acond (&rest clauses)
  (parse-cond-clauses 'acond 'aif clauses))

(defmacro scond (&rest clauses)
  (parse-cond-clauses 'scond 'sif clauses))

(defmacro acase (keyform &body cases)
  `(let ((it ,keyform))
     (cond ,@(mapcar #'parse-case-clause cases))))

(defmacro scase (keyform &body cases)
  `(symbol-macrolet ((it ,keyform))
     (cond ,@(mapcar #'parse-case-clause cases))))

(defmacro aecase (keyform &body cases)
  `(let ((it ,keyform))
     (cond ,@(mapcar #'parse-case-clause cases)
           (t (error 'type-error
                     :datum it
                     :expected-type '(member ,@(mappend (compose #'ensure-list #'first) cases)))))))

(defmacro secase (keyform &body cases)
  `(symbol-macrolet ((it ,keyform))
     (cond ,@(mapcar #'parse-case-clause cases)
           (t (error 'type-error
                     :datum it
                     :expected-type '(member ,@(mappend (compose #'ensure-list #'first) cases)))))))

(defmacro atypecase (keyform &body cases)
  `(let ((it ,keyform))
     (cond ,@(mapcar #'parse-typecase-clause cases))))

(defmacro stypecase (keyform &body cases)
  `(symbol-macrolet ((it ,keyform))
     (cond ,@(mapcar #'parse-typecase-clause cases))))

(defmacro aetypecase (keyform &body cases)
  `(let ((it ,keyform))
     (cond ,@(mapcar #'parse-typecase-clause cases)
           (t (error 'type-error
                     :datum it
                     :expected-type '(member ,@(mappend (compose #'ensure-list #'first) cases)))))))

(defmacro setypecase (keyform &body cases)
  `(symbol-macrolet ((it ,keyform))
     (cond ,@(mapcar #'parse-typecase-clause cases)
           (t (error 'type-error
                     :datum it
                     :expected-type '(member ,@(mappend (compose #'ensure-list #'first) cases)))))))


;;; SELF Anaphora
(defmacro alambda (args &body body)
  `(labels ((self ,args ,@body)) #'self))
