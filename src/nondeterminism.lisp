(defpackage assets.nondeterminism
  (:use :common-lisp)
  (:import-from :assets
                #:nd-let #:nd-let*)
  (:export #:nd-let #:nd-let*)
  )
(in-package :assets.nondeterminism)


;;; Util
(deftype index ()
  `(integer 0 ,array-dimension-limit))

(defun nlist? (n object)
  (check-type n index)
  (do ((i n (1- i))
       (obj object (rest obj)))
      ((zerop i) (null obj))
      (when (endp obj)
        (return nil))))

(declaim (inline compose))
(defun compose (g f)
  (lambda (&rest args)
    (funcall g (apply f args))))

(declaim (inline partial))
(defun partial (f &rest args)
  (lambda (&rest rest-args)
    (apply f (append args rest-args))))


;;; Helper
(defun parse-bindings (bindings body)
  (if (null bindings)
      body
      (let ((binding1 (first bindings))
            (bindings (rest bindings)))
        `(dolist (,(first binding1) ,(second binding1))
           ,(parse-bindings bindings body)))))


;;; Main
(defmacro nd-let (bindings &body body)
  (assert (every (partial #'nlist? 2) bindings) (bindings))
  (let* ((syms (mapcar #'first bindings))
         (exprs (mapcar #'second bindings))
         (gsyms (mapcar (compose #'gensym #'symbol-name) syms))
         (g!acc (gensym "G!ACC")))
    `(let ((,g!acc '()))
       (let ,(mapcar #'list gsyms exprs)
         ,(parse-bindings
            (mapcar #'list syms gsyms)
            `(push (progn ,@body) ,g!acc))
         (nreverse ,g!acc)))))

(defmacro nd-let* (bindings &body body)
  (assert (every (partial #'nlist? 2) bindings) (bindings))
  (let ((g!acc (gensym "G!ACC")))
    `(let ((,g!acc '()))
       ,(parse-bindings
          bindings
          `(push (progn ,@body) ,g!acc))
       (nreverse ,g!acc))))
