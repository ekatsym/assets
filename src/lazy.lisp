(defpackage assets.lazy
  (:use :common-lisp)
  (:import-from :assets
                #:promise
                #:delay
                #:force)
  (:export #:promise
           #:delay
           #:force))
(in-package :assets.lazy)

(defstruct (promise (:constructor promise (thunk)))
  (forced? nil)
  (cache '())
  (thunk thunk :type function))

(defmethod print-object ((object promise) stream)
  (if (promise-forced? object)
      (format stream "#<PROMISE~{ ~a~}>" (promise-cache object))
      (format stream "#<PROMISE>")))

(defmacro delay (&body body)
  `(promise (lambda () ,@body)))

(defgeneric force (promise)
  (:documentation "Evaluates PROMISE and returns the value."))

(defmethod force ((promise promise))
  (unless (promise-forced? promise)
      (with-slots (forced? cache thunk) promise
        (setf forced? t)
        (setf cache (multiple-value-list (funcall thunk)))))
  (values-list (promise-cache promise)))
