(defpackage assets
  (:use :common-lisp)
  (:export
    ;; defmacro
    #:defmacro!

    ;; list
    #:xcons #:ncons #:ensure-list #:nlist?
    #:enum #:make-nlist
    #:take #:drop #:sublist #:init
    #:foldl #:foldr #:unfoldl #:unfoldr
    #:random-nth #:random-list
    #:last1 #:append1
    #:length= #:length<
    #:mappend #:filter
    #:group #:flatten
    #:map-tree #:prune #:prune-if #:prune-if-not
    #:find-if2 #:after #:before #:duplicate?
    #:split #:split-if #:split-at
    #:best #:most #:mosts
    #:rotate #:nrotate #:shuffle

    ;; lazy
    #:promise #:delay #:force

    ;; llist
    #:llist #:lendp
    #:lcons #:lcar #:lcdr
    #:lfirst #:lrest
    #:llist->list #:list->llist
    #:lreverse #:lappend #:lrevappend
    #:ltake #:ldrop #:lsublist
    #:lnth #:linit #:llast
    #:lfoldl #:lfoldr #:lunfoldl #:lunfoldr
    #:lmap #:lfilter
    #:lremove #:lremove-if #:lremove-if-not
    #:lmember #:lmember-if
    #:lsplit #:lsplit-if #:lsplit-at
    #:ltree->tree #:tree->ltree

    ;; function
    #:compose #:composen
    #:partial #:rpartial
    #:curry #:rcurry
    #:pipe
    #:f-if #:f-and #:f-or
    #:recur #:trecur  

    ;; queue
    #:queue #:lqueue
    #:*qempty* #:*lqempty*
    #:qempty? #:qsnoc #:qfirst #:qrest

    ;; anaphora
    #:aif #:sif
    #:awhen #:swhen
    #:aunless #:sunless
    #:acond #:scond
    #:acase #:scase
    #:aecase #:secase
    #:atypecase #:stypecase
    #:aetypecase #:setypecase
    #:self #:alambda

    ;; nondeterminism
    #:nd-let #:nd-let*

    ;; comprehension
    #:list-comprehension
    #:in #:is #:for #:to

    ;; monad
    #:return-monad
    #:bind-monad
    #:do-monad
    #:maybe
    #:either))
