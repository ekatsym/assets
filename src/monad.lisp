(defpackage assets.monad
  (:use :common-lisp)
  (:import-from :assets
                #:return-monad
                #:bind-monad
                #:do-monad
                #:maybe
                #:either)
  (:export #:return-monad
           #:bind-monad
           #:do-monad
           #:identity
           #:maybe #:just #:nothing
           #:either #:left #:right
           #:list))
(in-package :assets.monad)


;;; Util
(deftype index ()
  `(integer 0 ,array-dimension-limit))

(defun nlist (n object)
  (check-type n index)
  (do ((i n (1- i))
       (lst object (rest lst)))
      ((zerop i) (null lst))
      (when (or (null lst)
                (not (listp lst)))
        (return nil))))

(declaim (inline ensure-list))
(defun ensure-list (x)
  (if (listp x) x (list x)))

(defun mappend (function list)
  (check-type function function)
  (check-type list list)
  (labels ((rec (lst acc)
             (declare (optimize (speed 3)))
             (if (endp lst)
                 (reduce (lambda (ac x) (append x ac)) acc)
                 (rec (rest lst)
                      (let ((fx (funcall function (first lst))))
                        (check-type fx list)
                        (cons fx acc))))))
    (rec list '())))


;;; return
(defgeneric return-monad (x monad)
  (:documentation "Return MONAD including x."))

;;; >>=
(defgeneric bind-monad (monad-a a->monad-b)
  (:documentation "Return MONAD-B."))


;; Do
(defmacro do-monad ((&rest bindings) &body body)
  (parse-bindings bindings body))

(defun parse-bindings (bindings body)
  (if (endp bindings)
      `(progn ,@body)
      (let ((binding (ensure-list (first bindings)))
            (bindings (rest bindings)))
        (cond ((nlist 1 binding)
               (let ((g!x (gensym)))
                 `(bind-monad ,@binding
                              (lambda (,g!x)
                                (declare (ignore ,g!x))
                                ,(parse-bindings bindings body)))))
              ((nlist 2 binding)
               `(bind-monad ,(second binding)
                            (lambda (,(first binding))
                              ,(parse-bindings bindings body))))
              (t
               (assert (or (nlist 1 binding)
                           (nlist 2 binding))
                       (binding)))))))


;;; Identity Monad
(defmethod return-monad (x (monad (eql 'identity)))
  x)

(defmethod bind-monad ((monad-a t) a->monad-b)
  (funcall a->monad-b monad-a))


;;; Maybe Monad
(defstruct maybe)
(defstruct (just (:include maybe)
                 (:constructor just (content)))
  content)
(defstruct (nothing (:include maybe)
                    (:constructor nothing)))

(defmethod return-monad (x (monad (eql 'maybe)))
  (just x))

(defmethod bind-monad ((monad-a maybe) a->monad-b)
  (if (just-p monad-a)
      (funcall a->monad-b (just-content monad-a))
      monad-a))


;;; Either Monad
(defstruct either)
(defstruct (left (:include either)
                 (:constructor left (content)))
  content)
(defstruct (right (:include either)
                  (:constructor right (content)))
  content)

(defmethod return-monad (x (monad (eql 'either)))
  (right x))

(defmethod bind-monad ((monad-a either) a->monad-b)
  (etypecase monad-a
    (right (funcall a->monad-b (right-content monad-a)))
    (left monad-a)))


;;; List Monad
(defmethod return-monad (x (monad (eql 'list)))
  (list x))

(defmethod bind-monad ((monad-a list) a->monad-b)
  (mappend a->monad-b monad-a))


#|
(do-monad ((a (just 3))
           (b (just 7))
           (c (just 11)))
  (return-monad (* a b c) 'maybe))
; => #S(JUST :CONTENT 231)

(do-monad ((a (just 3))
           (b (nothing))
           (c (just 11)))
  (return-monad (* a b c) 'maybe))
; => #S(NOTHING)

(do-monad ((a (right 3))
           (b (right 7))
           (c (right 11)))
  (return-monad (* a b c) 'either))
; => #S(RIGHT :CONTENT 231)

(do-monad ((a (right 3))
           (b (left 7))
           (c (right 11)))
  (return-monad (* a b c) 'either))
; => #S(LEFT :CONTENT 7)

(do-monad ((a (list 0 1 2 3))
           (b (list 10 20 30)))
  (return-monad (+ a b) 'list))
; => (10 20 30 11 21 31 12 13 23 33)
|#
