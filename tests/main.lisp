(defpackage assets/tests/main
  (:use :cl
        :assets
        :rove))
(in-package :assets/tests/main)

;; NOTE: To run this test file, execute `(asdf:test-system :assets)' in your Lisp.

(deftest test-target-1
  (testing "should (= 1 1) to be true"
    (ok (= 1 1))))
